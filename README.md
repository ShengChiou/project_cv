### Hi, I am Sheng. This is my CV site.

Site URL: https://shengchiou.gitlab.io/project_cv

#React #RxJs #Vite

#### dev environment and tool

---

- Mac ( M1 pro )
- node 16.15.1
- yarn 1.22.19
- react 18.2
- vite

#### setup step

---

1. download repo
2. `yarn`
3. `yarn dev`
