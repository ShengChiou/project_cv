import React from 'react';
import './style/main.scss';
import MainRoute from './app-route';

import { GlobalProvider } from '@store';
import Pop from '@share-components/Pop';

const App = () => {
  return (
    <GlobalProvider>
      <Pop />
      <MainRoute />
    </GlobalProvider>
  );
};

export default App;
