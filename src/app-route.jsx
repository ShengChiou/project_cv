import { Routes, Route } from 'react-router-dom';
import React, { Suspense } from 'react';
import CV from '@feature/CV/pages/CV';
import Layout from './layouts';

const MainRoute = () => {
  return (
    <Routes>
      <Route path={import.meta.env.BASE_URL} element={<Layout />}>
        <Route index element={<CV />} />
      </Route>
    </Routes>
  );
};

export default MainRoute;
