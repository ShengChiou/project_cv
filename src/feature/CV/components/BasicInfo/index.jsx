import React, { useEffect } from 'react';
import { useInView } from 'react-intersection-observer';

import PageCard from '@share-components/PageCard';
import JumpTxt from '@share-components/JumpTxt';
import { useGlobal } from '@store';
import { about } from '@info';

import { getNamePos } from '@CV/pages/CV';
import classNames from 'classnames/bind';
import style from './style.module.scss';
const cx = classNames.bind(style);

const { abstract } = about;

export const BasicInfo = () => {
  const { ref, inView } = useInView({});
  const { currentScroll$ } = useGlobal();

  useEffect(() => {
    if (!inView) return;
    currentScroll$.next(getNamePos('info')[1]);
  }, [inView]);

  return (
    <>
      <PageCard cardTitle={getNamePos('info')[0]} cardSub="About">
        <div className={cx('wrap')} ref={ref}>
          <div className={cx('main-title')}>
            <span>{`[ Sheng ]`}</span>
          </div>
          <div className={cx('des')}>{abstract}</div>
          <div className={cx('equation')}>
            <JumpTxt str={inView ? 'const I = bio => code => 邱穫升' : ''}></JumpTxt>
          </div>
          <div className={cx('box')}></div>
        </div>
      </PageCard>
    </>
  );
};
