import React, { Children, useEffect, useRef, useState } from 'react';
import { useInView } from 'react-intersection-observer';

import PageCard from '@share-components/PageCard';
import Button from '@share-components/Button';
import { useGlobal } from '@store';
import { contact, cv_url, copy_right } from '@info';

import { getNamePos } from '@CV/pages/CV';
import classNames from 'classnames/bind';
import style from './style.module.scss';
const cx = classNames.bind(style);

const handleViewCV = () => window.open(cv_url);

export const Contact = () => {
  const refImgWrap = useRef(null);
  const [copyId, setCopyId] = useState(null);
  const { ref, inView } = useInView({});
  const { currentScroll$ } = useGlobal();

  const copyTxt = (idx = 0) => {
    const [, infoTxt] = Object.entries(contact)[idx];
    try {
      navigator && navigator?.clipboard?.writeText(infoTxt);
      setCopyId(() => idx);
    } catch (err) {
      console.log('copy error', err);
    }
  };

  useEffect(() => {
    if (!inView) return;
    currentScroll$.next(getNamePos('ctat')[1]);
  }, [inView]);
  return (
    <>
      <PageCard cardTitle={getNamePos('ctat')[0]} cardSub="Contact">
        <div className={cx('wrap')} ref={ref}>
          <div className={cx('photo')} ref={refImgWrap}>
            {refImgWrap?.current && (
              <img
                src={`https://ik.imagekit.io/7ul2jfhwvqq/tr:ar-4-3,w-${refImgWrap?.current?.offsetWidth}/contact_cc7uQVQ8o`}
                alt="HuoSheng Chiou"
              />
            )}
          </div>
          <div className={cx('info-wrap')}>
            <div className={cx('info')}>
              {Children.toArray(
                Object.entries(contact).map((item, idx) => (
                  <>
                    <strong>{item[0]}</strong>
                    <div onClick={() => copyTxt(idx)}>
                      {` ${item[1]}`}
                      <span className={cx('copy-icon', copyId === idx && 'copied')}></span>
                    </div>
                  </>
                ))
              )}
            </div>
          </div>
          <div className={cx('btn-block')}>
            <div className={cx('cv-btn')}>
              <Button onClick={handleViewCV} name={'Open CV'} />
            </div>
          </div>
          <div className={cx('copy-r')}>{copy_right}</div>
        </div>
      </PageCard>
    </>
  );
};
