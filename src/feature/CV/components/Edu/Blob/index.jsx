import React, { useEffect, useRef, Children } from 'react';

import { interval } from 'rxjs';

import classNames from 'classnames/bind';
import style from './style.module.scss';
const cx = classNames.bind(style);

const animateTrigger$ = interval(6000);

let paths = [
  'M38.7,-46.5C53,-42.8,69.7,-35.4,77.6,-22.5C85.5,-9.6,84.7,8.8,76.3,21.8C68,34.8,52.1,42.3,38.1,49.6C24.2,56.8,12.1,63.6,-0.8,64.8C-13.8,66,-27.6,61.4,-40.4,53.9C-53.3,46.3,-65.2,35.7,-71.5,21.9C-77.8,8.1,-78.4,-8.8,-72.1,-21.9C-65.8,-35.1,-52.5,-44.5,-39.3,-48.7C-26.1,-52.8,-13.1,-51.7,-0.5,-51C12.1,-50.4,24.3,-50.3,38.7,-46.5Z',
  'M48.7,-64C62.8,-56.7,73.9,-42.1,77.4,-26.3C80.9,-10.5,76.9,6.6,70.2,21.5C63.6,36.4,54.3,49.2,42.1,59.5C30,69.9,15,77.9,1.3,76.1C-12.4,74.3,-24.8,62.8,-35.4,51.9C-46,41,-54.7,30.8,-62.6,17.7C-70.4,4.6,-77.4,-11.4,-74.8,-25.8C-72.2,-40.3,-60.1,-53.2,-46.1,-60.5C-32,-67.8,-16,-69.6,0.6,-70.5C17.3,-71.4,34.5,-71.3,48.7,-64Z',

  'M51.9,-74.3C64.8,-62.1,71.1,-43.6,75.9,-25.4C80.8,-7.3,84.2,10.6,77.5,23.3C70.8,36,53.9,43.5,39.3,47.1C24.7,50.8,12.3,50.6,0.6,49.7C-11,48.9,-22.1,47.2,-37.4,43.8C-52.7,40.4,-72.3,35.1,-79.2,23.5C-86.1,11.8,-80.4,-6.2,-73.7,-23C-66.9,-39.7,-59,-55,-46.6,-67.4C-34.1,-79.8,-17.1,-89.2,1.2,-90.9C19.5,-92.6,39,-86.6,51.9,-74.3Z',
];

const ratios = [1.2, 1.3, 1.4];

const rotates = [0, 110, 45];

const transformParams = [`translate(0 50)`, `translate(100 100)`, `translate(200, 100)`];

const Blob = ({ on = false }) => {
  const refPaths = useRef([]);
  const refSub = useRef(null);
  useEffect(() => {
    if (!on) {
      refSub.current && refSub.current.unsubscribe();
      return;
    }
    let state = 0;
    let updatePos = () => {
      if (state > 2) {
        state = 0;
      }
      for (const [i, dom] of refPaths.current.entries()) {
        dom.setAttribute('transform', `${transformParams[state]} rotate(${rotates[state]}) scale(${ratios[i]})`);
      }
      state++;
    };
    refSub.current = animateTrigger$.subscribe(updatePos);
    return () => {
      refSub.current && refSub.current.unsubscribe();
    };
  }, [on]);
  return (
    <div className={cx('heart-wrap')}>
      <svg viewBox="0 0 200 200" xmlns="hrefPathsp://www.w3.org/2000/svg">
        {Children.toArray(
          Array(3)
            .fill(null)
            .map((_, idx) => (
              <path
                ref={(el) => {
                  refPaths.current[idx] = el;
                }}
                fill={idx === 0 ? '#F2F4F8' : 'none'}
                stroke={idx !== 0 ? '#F2F4F8' : 'transparent'}
                strokeWidth="0.5"
                d={paths[0]}
                transform={`translate(0 50) scale(${0.35 * (idx + 1)})`}
              />
            ))
        )}
      </svg>
    </div>
  );
};

export default Blob;
