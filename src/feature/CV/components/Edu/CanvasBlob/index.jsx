import React, { useEffect, useRef, forwardRef } from 'react';

const CanvasBlob = ({ refP }) => {
  const refCvs = useRef(null);
  useEffect(() => {
    refCvs.current.setAttribute('touch-action', 'none');
    const resize = () => {
      //TODO container adjust
      refCvs.current.width = refP.current.clientWidth;
      refCvs.current.height = refP.current.clientHeight;
    };
    resize();
    window.addEventListener('resize', resize);
    const ctx = refCvs.current.getContext('2d');

    function getRandomArbitrary(min, max) {
      const amount = Math.random() * (max - min) + min;
      const sign = Math.random() < 0.4 ? -1 : 1;
      return sign * amount;
    }
    function rint(min, max) {
      const amount = Math.random() * (max - min) + min;
      return Math.round(amount);
    }

    // Getting points needed to draw the circle
    function getCirclePoints(base, radius) {
      const angles = [
        // angles at which we compute points
        rint(0, 90 - 45),
        rint(90, 180 - 45),
        rint(180, 270 - 45),
        rint(270, 360 - 45),
      ];
      const positions = [];
      for (let a in angles) {
        const angle = (angles[a] * Math.PI) / 180;
        let ba = ((angles[a] - 20) * Math.PI) / 180;
        let rr = radius + getRandomArbitrary(40, 100);
        positions.push({
          x: base.x + radius * Math.sin(angle),
          y: base.y + radius * Math.cos(angle),
          mx: base.x + rr * Math.sin(ba),
          my: base.y + rr * Math.cos(ba),
        });
      }
      positions.push(positions[0]); // last one is same as first to make sure they line up
      return positions;
    }
    function drawPath(points) {
      let cpath = `M${points[0].x},${points[1].y}`;
      for (let point of points) cpath += `S${point.mx},${point.my},${point.x},${point.y}`;
      cpath += 'Z';
      let p = new Path2D(cpath);
      ctx.clearRect(0, 0, refCvs.current.width, refCvs.current.height);
      ctx.fillStyle = 'rgb(229, 244, 216)';
      ctx.fill(p);
    }
    drawPath(getCirclePoints({ x: 200, y: 100 }, 50));
  }, []);
  return <canvas ref={refCvs}></canvas>;
};

export default CanvasBlob;
