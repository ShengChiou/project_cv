import React, { Children, useEffect } from 'react';
import { useInView } from 'react-intersection-observer';
import { isMobile } from 'react-device-detect';

import { edu } from '@info';
import { useGlobal } from '@store';
import JumpTxt from '@share-components/JumpTxt';
import PageCard from '@share-components/PageCard';

import Blob from './Blob';
import { getNamePos } from '@CV/pages/CV';
import classNames from 'classnames/bind';
import style from './style.module.scss';
const cx = classNames.bind(style);

export const Edu = () => {
  const { ref, inView } = useInView({});
  const { currentScroll$ } = useGlobal();

  useEffect(() => {
    if (!inView) return;
    currentScroll$.next(getNamePos('edu')[1]);
  }, [inView]);

  return (
    <>
      <PageCard cardTitle={getNamePos('edu')[0]} cardSub="Ｅducation">
        <div className={cx('wrap')} ref={ref}>
          {!isMobile && <Blob on={inView} />}

          <div className={cx('des-wrap')}>
            {Children.toArray(
              edu.map((item) => (
                <>
                  <div className={cx('edu-unit')}>
                    <div className={cx('ym')}>
                      <p>{item.e_t.join(' ')}</p>
                      <p>{item.s_t.join(' ')}</p>
                    </div>
                    <div className={cx('title')}>
                      <JumpTxt str={inView ? item.dep : ''} />
                      <JumpTxt str={inView ? item.deg : ''} />
                      <span className={cx('place')}>{item.place}</span>
                    </div>
                  </div>
                </>
              ))
            )}
          </div>
        </div>
      </PageCard>
    </>
  );
};
