import React, { Children } from 'react';

import classNames from 'classnames/bind';
import style from './style.module.scss';
const cx = classNames.bind(style);

const MobPop = ({ data = null }) => {
  if (!data) return;
  const { job_des } = data;
  return (
    <div className={cx('pop-ctx')}>
      {Children.toArray(
        job_des.map((des, idx) => (
          <>
            <div className={cx('count')}></div>
            <div className={cx('work')}>{des}</div>
          </>
        ))
      )}
    </div>
  );
};

export default MobPop;
