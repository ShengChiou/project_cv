import React, { Children, useEffect } from 'react';
import { useInView } from 'react-intersection-observer';
import { isMobile } from 'react-device-detect';

import PageCard from '@share-components/PageCard';
import ExpCard from '@share-components/ExpCard';
import { exp } from '@info';
import { useGlobal } from '@store';

import MobPop from './components/MobPop';
import { getNamePos } from '@CV/pages/CV';
import classNames from 'classnames/bind';
import style from './style.module.scss';
const cx = classNames.bind(style);

export const Exp = () => {
  const { ref, inView } = useInView({});
  const { currentScroll$, pop } = useGlobal();

  useEffect(() => {
    if (!inView) return;
    currentScroll$.next(getNamePos('exp')[1]);
  }, [inView]);

  return (
    <>
      <PageCard cardTitle={getNamePos('exp')[0]} cardSub="Work experience">
        <div className={cx('wrap')} ref={ref}>
          {Children.toArray(
            exp.map((item) =>
              isMobile ? (
                <>
                  <div className={cx('job-meta')}>
                    <div className={cx('job-time')}>{`${item.time[0]} ${exp[0].time[1]}`}</div>
                    <div className={cx('des')}>
                      <div className={cx('job')}>
                        <div>{item.comp_name}</div>
                        <div>{item.job_title}</div>
                      </div>
                      <div
                        className={cx('detail-btn')}
                        onClick={() => pop({ C: <MobPop data={item} />, on: true })}
                      ></div>
                    </div>
                  </div>
                </>
              ) : (
                <>
                  <ExpCard {...item} />
                </>
              )
            )
          )}
        </div>
      </PageCard>
    </>
  );
};
