import React, { useEffect } from 'react';
import { useInView } from 'react-intersection-observer';

import Wave from '@share-components/Wave';
import CC from '@share-components/Concentric';
import ScrollIcon from '@share-components/ScrollIcon';
import JumpTxt from '@share-components/JumpTxt';
import { useGlobal } from '@store';

import { getNamePos } from '@CV/pages/CV';
import classNames from 'classnames/bind';
import style from './style.module.scss';
const cx = classNames.bind(style);

export const Land = () => {
  const { ref, inView } = useInView({});
  const { currentScroll$ } = useGlobal();

  useEffect(() => {
    if (!inView) return;
    currentScroll$.next(getNamePos('land')[1]);
  }, [inView]);

  return (
    <div className={cx('wrap')}>
      <div className={cx('wave-1')}>
        <Wave on={inView} />
      </div>
      <div className={cx('site-name')} ref={ref}>
        <JumpTxt str={inView ? 'PROJECT CV' : ''}></JumpTxt>
      </div>
      <div className={cx('author')}>
        <JumpTxt str={inView ? 'HUOSHENG CHIOU' : ''}></JumpTxt>
      </div>
      <div className={cx('circle-block')}>
        <div className={cx('circle', 'c1')}>
          <CC />
        </div>
        <div className={cx('circle', 'c2')}>
          <CC baseR={10} num={10} diff={15} />
        </div>
        <div className={cx('circle', 'c3')}>
          <CC baseR={10} num={10} diff={15} />
          <CC baseR={10} num={8} diff={15} />
        </div>
        <div className={cx('circle', 'c4')}>
          <CC baseR={10} num={10} diff={15} />
        </div>
      </div>
      <div className={cx('icon')}>
        <ScrollIcon />
      </div>
    </div>
  );
};
