import React, { Children } from 'react';

import classNames from 'classnames/bind';
import style from './style.module.scss';
const cx = classNames.bind(style);

const SkillCard = ({ data = null }) => {
  if (!data) return null;
  const [title, skills] = data;
  return (
    <div className={cx('edge')}>
      <div className={cx('head')}>
        <div className={cx('title')}>{title}</div>
      </div>
      <div className={cx('content')}>
        {Children.toArray(
          skills.map((g) => (
            <div className={cx('skill-line')}>{Children.toArray(g.map((skill) => <span>{skill}</span>))}</div>
          ))
        )}
      </div>
    </div>
  );
};

export default SkillCard;
