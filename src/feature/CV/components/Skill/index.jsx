import React, { Children, useEffect } from 'react';
import { useInView } from 'react-intersection-observer';

import D3C from '@share-components/CarouselThreeD';
import PageCard from '@share-components/PageCard';
import { skill } from '@info';
import { useGlobal } from '@store';

import { getNamePos } from '@CV/pages/CV';
import SkillCard from './components/SkillCard';
import classNames from 'classnames/bind';
import style from './style.module.scss';
const cx = classNames.bind(style);

export const Skill = () => {
  const { ref, inView } = useInView({});
  const { currentScroll$ } = useGlobal();

  useEffect(() => {
    if (!inView) return;
    currentScroll$.next(getNamePos('skill')[1]);
  }, [inView]);
  return (
    <>
      <PageCard cardTitle={getNamePos('skill')[0]} cardSub="Skill">
        <div className={cx('wrap')} ref={ref}>
          <D3C>
            {Children.toArray(
              Object.entries(skill).map((item) => (
                <div className={cx('skillCard-wrap')}>
                  <SkillCard data={item} />
                </div>
              ))
            )}
          </D3C>
        </div>
      </PageCard>
    </>
  );
};
