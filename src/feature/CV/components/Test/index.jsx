import React, { Children, useState } from 'react';

import classNames from 'classnames/bind';
import style from './style.module.scss';
const cx = classNames.bind(style);

import SkillCard from '@CV/components/Skill/components/SkillCard';
import PageCard from '@share-components/PageCard';

//TODO add key left right
export const Test = () => {
  const arrData = [1, 2, 3];
  const [select, setSelect] = useState(() => arrData.length);

  const assignPos = (arr, idx) =>
    setSelect((pre) => {
      //TODO adjust for next back loop
      if ((pre + idx) % 3 === 0) return pre;
      return -idx + arr.length;
    });

  return (
    <>
      <PageCard cardTitle="測試頁" cardSub="Test">
        {Children.toArray(
          arrData.map((_, idx) => (
            <>
              <button onClick={() => assignPos(arrData, idx)}>{idx + 1}</button>
            </>
          ))
        )}
        <div className={cx('wrap')}>
          <div className={cx('scene')}>
            <div className={cx('btn', 'left')} onClick={() => setSelect((pre) => pre + 1)}></div>
            <div className={cx('btn', 'right')} onClick={() => setSelect((pre) => pre - 1)}></div>
            <div className={cx('carousel')}>
              {Children.toArray(
                arrData.map((item, idx) => (
                  <>
                    <div
                      style={{
                        transform: `rotateY(${(idx + select) * (360 / arrData.length)}deg) translateZ(288px)`,
                      }}
                      className={cx('carousel-cell')}
                    >
                      <div className={cx('skill-card')}>
                        <SkillCard />
                      </div>
                    </div>
                  </>
                ))
              )}
            </div>
          </div>
          {/* <div className={cx('window')}>
            <div className={cx('stage')}>
              {Array(3)
                .fill(null)
                .map((_, idx) => (
                  <div className={cx(`card${idx}`, cx('card'))}></div>
                ))}
            </div>
          </div> */}
        </div>
      </PageCard>
    </>
  );
};
