export * from './Land';
export * from './BasicInfo';
export * from './Exp';
export * from './Skill';
export * from './Edu';
export * from './Contact';
export * from './Test';
