import React, { useRef, Children } from 'react';
import { isMobile } from 'react-device-detect';

import { tap, distinctUntilChanged } from 'rxjs/operators';

import { useGlobal } from '@store';
import useSubscription from '@hooks/useSubscription';
import { Land, BasicInfo, Exp, Skill, Edu, Contact, Test } from '@feature/CV/components';

import classNames from 'classnames/bind';
import style from './style.module.scss';
const cx = classNames.bind(style);

export const blocks = {
  land: ['land', <Land />],
  info: ['關於', <BasicInfo />],
  edu: ['學歷', <Edu />],
  exp: ['經歷', <Exp />],
  skill: ['技能', <Skill />],
  ctat: ['聯繫', <Contact />],
};

const NamePosMap = Object.entries(blocks).reduce((pre, block, idx) => {
  pre[`${block[0]}`] = [block[1][0], idx];
  return pre;
}, {});
export const displayBlocks = Object.entries(blocks).map((item) => item[1][1]);

//[name idx]
export const getNamePos = (code) => NamePosMap[code];

const CV = () => {
  const refBlocks = useRef([]);
  const { tabTo$ } = useGlobal();
  useSubscription(
    tabTo$.pipe(
      distinctUntilChanged(),
      tap((v) => {
        if (v === null || v === undefined) return;
        refBlocks.current[v].scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'end' });
      })
    )
  );

  return (
    <>
      <div className={cx('cv-wrap')}>
        {Children.map(displayBlocks, (child) => (
          <section ref={(el) => refBlocks.current.push(el)}>{child}</section>
        ))}
      </div>
    </>
  );
};

export default CV;
