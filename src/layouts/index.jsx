import React from 'react';
import Nav from '@share-components/Nav';
import { Outlet } from 'react-router-dom';

const index = () => {
  return (
    <>
      <Nav></Nav>
      <Outlet />
    </>
  );
};

export default index;
