import React from 'react';

import classNames from 'classnames/bind';
import style from './style.module.scss';
const cx = classNames.bind(style);

const Button = (props) => {
  const { name, wrapClass, ...others } = props;
  return (
    <button className={cx('btn', wrapClass)} {...others}>
      {name}
    </button>
  );
};

export default Button;
