import React, { Children, useState } from 'react';

import arrow from '/svg/arrow.svg';
import classNames from 'classnames/bind';
import style from './style.module.scss';
const cx = classNames.bind(style);

const CarouselThreeD = ({ children }) => {
  const [select, setSelect] = useState(() => children.length);
  const assignPos = (arr = [], idx) =>
    setSelect((pre) => {
      if ((pre + idx) % 3 === 0) return pre;
      return -idx + arr.length;
    });

  return (
    <>
      <div className={cx('wrap')}>
        <div className={cx('scene')}>
          <div className={cx('btn', 'left')} onClick={() => setSelect((pre) => pre + 1)}>
            <img src={arrow} alt="" />
          </div>
          <div className={cx('btn', 'right')} onClick={() => setSelect((pre) => pre - 1)}>
            <img src={arrow} alt="" />
          </div>
          <div className={cx('carousel')}>
            {Children.map(children, (child, idx) => (
              <>
                <div
                  style={{
                    transform: `rotateY(${(idx + select) * (360 / children.length)}deg) translateZ(288px)`,
                  }}
                  className={cx('carousel-cell')}
                >
                  {child}
                </div>
              </>
            ))}
          </div>
        </div>
      </div>
    </>
  );
};

export default CarouselThreeD;
