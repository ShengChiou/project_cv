import React, { Children } from 'react';

import classNames from 'classnames/bind';
import style from './style.module.scss';
const cx = classNames.bind(style);

const Concentric = ({ baseR = 10, num = 5, diff = 10 }) => {
  return (
    <svg
      className={cx('cc-svg')}
      style={{ width: `${(baseR + diff) * num + 50}px`, height: `${(baseR + diff) * num + 50}px` }}
    >
      {Children.toArray(
        Array(num)
          .fill(null)
          .map((_, idx) => {
            return <circle cx="50%" cy="50%" r={`${baseR + idx * diff}`} strokeWidth="2" fill="transparent" />;
          })
      )}
    </svg>
  );
};

export default Concentric;
