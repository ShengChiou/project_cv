import React, { Children } from 'react';

import classNames from 'classnames/bind';
import style from './style.module.scss';
const cx = classNames.bind(style);

const ExpCard = ({ comp_name, job_title, time, job_des }) => {
  return (
    <div className={cx('exp-card')}>
      <section className={cx('l')}>
        <div className={cx('title')}>{`${comp_name} ${job_title}`}</div>
        <div className={cx('time')}>
          <div>{time[0]}</div>
          <div>{time[1]}</div>
        </div>
      </section>
      <section className={cx('r')}>{Children.toArray(job_des.map((item) => <div>{item}</div>))}</section>
    </div>
  );
};

export default ExpCard;
