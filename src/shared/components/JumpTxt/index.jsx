import React, { useEffect, useState, useRef } from 'react';

const getRandomLabel = () => {
  const random = 'ΦΩξλψθΛΣςοξ';
  return random.charAt(Math.floor(Math.random() * 10));
};

const JumpTxt = ({ str }) => {
  const [txt, setTxt] = useState(() => str);
  const processing = useRef(false);

  const jumpText2 = (temp = [], init = [], pos = 0) => {
    processing.current = true;
    const jumpTimes = 3;
    let jumpNum = 0;

    if (pos === init.length) {
      processing.current = false;
      return;
    }
    const inter = setInterval(() => {
      if (jumpNum === jumpTimes - 1) {
        clearInterval(inter);
        temp[pos] = init[pos];
        setTxt(() => temp.join(''));
        pos++;
        jumpText2(temp, init, pos);
        return;
      }
      temp = init.map((_, idx) => (idx > pos ? getRandomLabel() : _));
      setTxt(() => temp.join(''));
      jumpNum++;
    }, 20);
  };

  useEffect(() => {
    if (!Boolean(str)) return;
    jumpText2([], str.split(''));
  }, [str]);

  const handleEnter = (e) => {
    if (processing.current) return;
    jumpText2([], str.split(''));
  };

  const handleLv = (e) => {
    console.log({ e });
  };
  return (
    <span onMouseEnter={handleEnter} onMouseLeave={handleLv}>
      {txt}
    </span>
  );
};

export default JumpTxt;
