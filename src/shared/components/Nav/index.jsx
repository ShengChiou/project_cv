import React, { Children } from 'react';
import { distinctUntilChanged } from 'rxjs/operators';

import { useGlobal } from '@store';
import useObservable from '@hooks/useObservable';

import { blocks } from '@CV/pages/CV';

import classNames from 'classnames/bind';
import style from './style.module.scss';
const cx = classNames.bind(style);

const Nav = () => {
  const { currentScroll$, tabTo$ } = useGlobal();
  const currentBlock = useObservable(currentScroll$.pipe(distinctUntilChanged()), null);
  if (!currentBlock) return null;

  return (
    <div className={cx('wrap')}>
      {Children.toArray(
        Object.entries(blocks).map((item, idx) =>
          !idx ? null : (
            <div className={cx('tab', currentBlock === idx && 'on')} onClick={() => tabTo$.next(idx)}>
              {item[1][0]}
            </div>
          )
        )
      )}
    </div>
  );
};

export default Nav;
