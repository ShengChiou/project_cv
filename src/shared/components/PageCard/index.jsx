import React from "react";
import classNames from "classnames/bind";
import style from "./style.module.scss";
const cx = classNames.bind(style);

const PageCard = ({ children, cardTitle, cardSub }) => {
  return (
    <>
      <div className={cx("wrap")}>
        <div className={cx("page-card")}>
          <div className={cx("top")}>
            <div className={cx("mark")}>
              <div className={cx("left-line")}></div>
              <div className={cx("title")} sub-txt={cardSub}>
                {cardTitle}
              </div>
              <div className={cx("right-line")}></div>
            </div>
          </div>
          <div className={cx("content")}>{children}</div>
        </div>
      </div>
    </>
  );
};

export default PageCard;
