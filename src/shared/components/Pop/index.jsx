import React from 'react';

import { useGlobal } from '@store';
import useObservable from '@hooks/useObservable';

import classNames from 'classnames/bind';
import style from './style.module.scss';
const cx = classNames.bind(style);

const Pop = () => {
  const { popData$, pop } = useGlobal();
  const comp = useObservable(popData$, null);
  if (!comp || !comp?.on) return null;
  const { C } = comp;
  return (
    <div className={cx('pop-edge')}>
      <div className={cx('pop-ctx-wrap')}>
        <div className={cx('close')} onClick={() => pop({ on: false })}></div>
        <div className={cx('ctx-wrap')}>{C}</div>
      </div>
    </div>
  );
};

export default Pop;
