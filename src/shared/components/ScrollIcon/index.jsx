import React from 'react';

import classNames from 'classnames/bind';
import style from './style.module.scss';
const cx = classNames.bind(style);

const ScrollIcon = () => {
  return (
    <div className={cx('scroll-downs')}>
      <div className={cx('mousey')}>
        <div className={cx('scroller')}></div>
      </div>
    </div>
  );
};

export default ScrollIcon;
