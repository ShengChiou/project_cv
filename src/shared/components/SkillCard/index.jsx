import React, { Children, memo } from 'react';

import { skill } from '@info';
const { front, backend, more } = skill;

import classNames from 'classnames/bind';
import style from './style.module.scss';
const cx = classNames.bind(style);

//filp card unuse now
const SkillCard = ({ mode }) => {
  const titleMap = {
    0: 'Front',
    1: 'Backend',
    2: 'More',
  };

  const getData = () => {
    switch (mode) {
      case 0:
        return front;
      case 1:
        return backend;
      case 2:
        return more;
      default:
        return [];
    }
  };

  return (
    <>
      <div className={cx('skill-card')}>
        <div className={cx('card-content')}>
          <div className={cx('front')}>
            <div className={cx('topic')}>{`[${titleMap[mode]}]`}</div>
          </div>
          <div className={cx('back')}>
            <div className={cx('back-content')}>
              {Children.toArray(
                getData().map((item, idx) => (
                  <>
                    <div className={cx('unit')}>
                      {Children.toArray(
                        item.map((tech) => (
                          <>
                            <div>{tech}</div>
                          </>
                        ))
                      )}
                    </div>
                    {getData().length !== idx + 1 && (
                      <div className={cx('seperate')}>
                        <div className={cx('comma')} />
                      </div>
                    )}
                  </>
                ))
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default memo(SkillCard);
