import React, { useEffect, useRef, Children } from 'react';
import { isMobile } from 'react-device-detect';

import classNames from 'classnames/bind';
import style from './style.module.scss';
const cx = classNames.bind(style);

const Wave = ({ num = 5, on = true }) => {
  const refSvg = useRef(null);
  const refWaves = useRef([]);
  const refAnimate = useRef(null); //random id instance
  useEffect(() => {
    if (!on) {
      refAnimate.current && cancelAnimationFrame(refAnimate.current);
      return;
    }
    let x_pos = [];
    let t = 0;
    for (let i = 0; i <= refSvg.current.clientHeight; i++) {
      x_pos.push(i);
    }
    const animate = () => {
      let points = x_pos.map((x) => {
        // center
        let svgHeight = refSvg.current.clientWidth / 2;
        //y 震幅100
        let y = svgHeight + 40 * Math.sin((x + t) / 20);
        return [x * 2, y];
      });

      for (const [i, path] of refWaves.current.entries()) {
        path.setAttribute('d', `M${points.map((p) => `${p[1] + 18 * i},${p[0]}`).join(' L')}`);
      }
      t += 0.05;
      refAnimate.current = requestAnimationFrame(animate);
    };
    animate();
  }, [on]);
  return (
    <>
      <svg ref={refSvg} className={cx('wave-svg')} height={`${document.customVh * 100 + (isMobile ? 1000 : 80)}px`}>
        {Children.toArray(
          Array(num)
            .fill(null)
            .map((_, idx) => (
              <path
                className={cx('wave-path')}
                ref={(el) => {
                  refWaves.current[idx] = el;
                }}
                d=""
              ></path>
            ))
        )}
      </svg>
    </>
  );
};

export default Wave;
