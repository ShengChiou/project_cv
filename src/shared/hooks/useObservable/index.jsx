import { useEffect, useState } from 'react';
import useSubscription from '../useSubscription';

const useObservable = (source$ = null, initialState, handleErr) => {
  const [val, setVal] = useState(() => initialState);
  useSubscription(source$, setVal, handleErr);
  return val;
};

export default useObservable;
