import { useEffect } from 'react';

const useSubscription = (source$ = null, handle = () => {}, handleErr = () => {}) => {
  useEffect(() => {
    if (!source$) return;
    const sub = source$.subscribe({ next: handle, error: handleErr });
    return () => {
      sub.unsubscribe();
    };
  }, [source$]);
};

export default useSubscription;
