import React, { createContext, useContext } from 'react';
import { BehaviorSubject } from 'rxjs';

//block idx
const currentScroll$ = new BehaviorSubject(null);
const tabTo$ = new BehaviorSubject(null);

const popData$ = new BehaviorSubject(null);
const pop = (data) => popData$.next(data);

const GlobalCtx = createContext();

export const GlobalProvider = ({ children }) => {
  return <GlobalCtx.Provider value={{ currentScroll$, tabTo$, popData$, pop }}>{children}</GlobalCtx.Provider>;
};
export const useGlobal = () => useContext(GlobalCtx);
