const setVh = () => {
  let vh =
    ((document && document.documentElement && document.documentElement.clientHeight) || window.innerHeight) * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
  document.customVh = vh;
};

window.addEventListener('resize', setVh());

setVh();
