import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

import path from 'path'; //adjust  require => from (vite3)

export default defineConfig(({ command, mode }) => ({
  // local dev port
  base: command === 'build' ? '/project_cv/' : '/',
  publicDir: 'src/assets/',
  css: {
    preprocessorOptions: {
      scss: {},
    },
  },
  resolve: {
    alias: {
      '@share-components': path.resolve(__dirname, 'src/shared/components'),
      '@feature': path.resolve(__dirname, 'src/feature'),
      '@assets': path.resolve(__dirname, 'src/assets'),
      '@components': path.resolve(__dirname, 'src/shared/components'),
      '@CV': path.resolve(__dirname, 'src/feature/CV'),
      '@store': path.resolve(__dirname, 'src/store.jsx'),
      '@hooks': path.resolve(__dirname, 'src/shared/hooks'),
      '@style': path.resolve(__dirname, 'src/style'),
      '@info': path.resolve(__dirname, 'src/info.json'),
      '/img': path.resolve(__dirname, 'src/assets'),
    },
  },
  build: {
    // outDir: 'dist/project_cv',
    emptyOutDir: true,
  },
  plugins: [react()],
}));
